var Album = Backbone.Model.extend({
    default: {
        title: ''
    }
});

var Albums = Backbone.Collection.extend({
    url: "https://rallycoding.herokuapp.com/api/music_albums",
    model: Album
});


var AlbumView = Backbone.View.extend({

    el: '#albumy',

    initialize: function () {
        this.collection = new Albums();

        $.when(this.collection.fetch({
                success: function () {
                    console.log("pobrano dane")
                },
                error: function () {
                    console.log("bład")
                }
            })
        ).then(function () {
                this.render();
            }.bind(this)
        )

    },
    render: function () {

        console.log("renderuje")

        var html = "";

        this.collection.each(function (log) {
            console.log('log item.', log);
            html += log.attributes.title + "<hr/>"
        });

        this.$el.html(html);
        return this;
    }
});


$(document).ready(function () {
    console.log('ready.');
    new AlbumView();
});