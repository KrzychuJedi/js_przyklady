//przyklad modelu
var ksiazki = Backbone.Model.extend({
    initialize: function () {
        //konstruktor modelu
        console.log("konstruktor ksiazki model utowrzono");
    },
    //domyslna struktura danych
    defaults: {
        id: 0,
        Nazwa: "Nazwa Ksiazki",
        Autor: "Autor",
        DataDodania: "22-10-2017",
        Opis: "Piękna książka",
    }
});

//przyklad kolekcji
var szafka = Backbone.Collection.extend({
    //zdefiniowanie modelu w kolekcji
    model: ksiazki

})
//utworzenie kolekcji w raz z danymi
//var szafkaColection = new szafka(new ksiazki
//                                 ({Id:1,
//                                   Nazwa:"moja ksiazka 1",
//                                   Autor:"ŁK",
//                                   DataDodania:"11-04-2017",
//                                   Opis:"MOja Ksiażka",
//
//                                  }));
var ksiazka1 = new ksiazki({
    id: 1,
    Nazwa: "Pan Tadeusza",
    Autor: "Adam Mickiewicz",
    DataDodania: "11-04-2017",
    Opis: "Pan Tadeusz, czyli Ostatni zajazd na Litwie – poemat epicki Adama Mickiewicza wydany w dwóch tomach w 1834 w Paryżu przez Aleksandra Jełowickiego. Ta epopeja narodowa z elementami gawędy szlacheckiej powstała w latach 1832–1834 w Paryżu."
});
var ksiazka2 = new ksiazki(
    {
        id: 2,
        Nazwa: "JavaScript podstawy",
        Autor: "Marijn Haverbeke",
        DataDodania: "05-04-2017",
        Opis: "Opanuj język internetu Współczesne aplikacje i strony internetowe nie byłyby takie same, gdyby nie potencjał języka JavaScript. Ten język programowania jeszcze kilka lat temu przeżywał kryzys, lecz w końcu został doceniony. Obecnie jest on stosowany wszędzie tam, gdzie użytkownicy wymagają najwyższej interaktywności, szybkości działania oraz wygody korzystania z aplikacji internetowej. Jeżeli chcesz poznać JavaScript i użyć go już w najbliższym projekcie, to trafiłeś na doskonałą książkę."
    });

var szafkaColection = new szafka();
szafkaColection.add(ksiazka1);
szafkaColection.add(ksiazka2);
var stolikDoKsiazek = Backbone.View.extend({
    //miejsce wyrenderowania danych
    el: "#books",
    //konstruktor danych
    initialize: function () {

        // funkcja odpowiedzialna za renderowanied danych
        this.render();
    },
    events: {
        "click #remove": "usun"
    },
    render: function () {
        //Pobranie wyglądu do napełniania danymi
        var source = $('#ksiazki').html();
        //podstawienie wyglądu do obsługi przez biblioteke
        var template = Handlebars.compile(source);
        //napełnienie templatki danymi
        var html = template(
            //zamienienie danych z utworzonej kolekcji na obiekt JSON
            szafkaColection.toJSON()
        );
        //wyrenderowanie tamplatki wraz z danymi
        this.$el.html(html);
        return this;
    },
    usun: function (ev) {
        var m = szafkaColection.where({id: $(ev.currentTarget).data("id")});
        szafkaColection.remove(m);

        st.render();
    }

});
var DodajKsiazkeView = Backbone.View.extend({
    //miejsce wyrenderowania danych
    el: "#books",
    //konstruktor danych
    initialize: function () {

        // funkcja odpowiedzialna za renderowanied danych
        this.render();
    },
    events: {
        "click #dodaj": "dodaj"
    },
    render: function () {

        //Pobranie wyglądu do napełniania danymi
        var source = $('#dodajKsiazke').html();
        //podstawienie wyglądu do obsługi przez biblioteke
        var template = Handlebars.compile(source);
        //napełnienie templatki danymi

        //wyrenderowanie tamplatki w raz z danymi
        this.$el.html(template);
        $(".bg-danger").hide();
        return this;
    },
    dodaj: function (e) {
        e.preventDefault();
        e.stopPropagation();
        var valid = true;
        if (this.$('#Nazwa').val() === "") {
            $('#Nazwa').parent().addClass("has-error");
            $(".nazwaWarning").show();
            valid = false;
        } else {
            $('#Nazwa').parent().removeClass("has-error");
            $(".nazwaWarning").hide();
        }

        if (this.$('#Autor').val() === "") {
            $('#Autor').parent().addClass("has-error");
            $(".autorlWarning").show();
            valid = false;
        } else {
            $('#Nazwa').parent().removeClass("has-error");
            $(".autorlWarning").hide();
        }

        if (this.$('#Opis').val() === "") {
            $('#Opis').parent().addClass("has-error");
            $(".OpisWarning").show();
            valid = false;
        } else {
            $('#Nazwa').parent().removeClass("has-error");
            $(".OpisWarning").hide();
        }

        if (valid) {
            var currentDate = new Date();
            var data = currentDate.getDate() + "-" + currentDate.getMonth() + "-" + currentDate.getFullYear();
            var ksiazka = new ksiazki(
                {
                    id: (szafkaColection.last() != undefined) ? szafkaColection.last().attributes.id + 1 : 1,
                    Nazwa: $('#Nazwa').val(),
                    Autor: $('#Autor').val(),
                    DataDodania: data,
                    Opis: $('#Opis').val()
                });
            szafkaColection.add(ksiazka);

            this.undelegateEvents();

            location.href = "#";
        }

    }

});
var EdytujKsiazkeView = Backbone.View.extend({
    //miejsce wyrenderowania danych
    el: "#books",
    idv: 0,
    //konstruktor danych
    initialize: function (id) {

        // funkcja odpowiedzialna za renderowanied danych
        this.render(id);
    },
    events: {
        "click #edytuj": "edytuj"
    },
    render: function (idd) {
        idv = idd;
        var m = szafkaColection.where({id: parseInt(idd)});
        //Pobranie wyglądu do napełniania danymi
        var source = $('#edytujKsiazke').html();
        //podstawienie wyglądu do obsługi przez biblioteke
        var template = Handlebars.compile(source);
        //napełnienie templatki danymi
        var html = template(
            //zamienienie danych z utworzonej kolekcji na obiekt JSON
            m[0].toJSON()
        );
        //wyrenderowanie tamplatki w raz z danymi
        this.$el.html(html);
        $(".bg-danger").hide();
        return this;
    },
    edytuj: function (e) {
        e.preventDefault();
        e.stopPropagation();
        var valid = true;
        if (this.$('#Nazwa').val() == "") {
            $('#Nazwa').parent().addClass("has-error");
            $(".nazwaWarning").show();
            valid = false;
        }
        if (this.$('#Autor').val() == "") {
            $('#Autor').parent().addClass("has-error");
            $(".autorlWarning").show();
            valid = false;
        }
        if (this.$('#Opis').val() == "") {
            $('#Opis').parent().addClass("has-error");
            $(".OpisWarning").show();
            valid = false;
        }
        if (valid) {
            szafkaColection.where({id: parseInt(idv)})[0].attributes.Nazwa = $('#Nazwa').val();
            szafkaColection.where({id: parseInt(idv)})[0].attributes.Autor = $('#Autor').val();
            szafkaColection.where({id: parseInt(idv)})[0].attributes.Opis = $('#Opis').val();
        }

        this.undelegateEvents();

        location.href = "#";
    }

});
var st = new stolikDoKsiazek();
var Czytalenia = Backbone.Router.extend({
    routes: {
        "": "index",
        "edytuj/:id": "edytuj",  // #search/cars
        "dodajKsiazke": "dodajKsiazke"

    },
    index: function () {
        st.render();
    },
    dodajKsiazke: function () {
        new DodajKsiazkeView();
    }
});


// Instantiate the router
var app_router = new Czytalenia;
app_router.on('route:edytuj', function (id) {
    new EdytujKsiazkeView(id)
});


Backbone.history.start();
//odpalenie fukcji.
