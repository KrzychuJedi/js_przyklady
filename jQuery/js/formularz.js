// $(function() { ... }); = $(document).ready(function() { ... });

$(function () {
    var source = $('#formularz').html();
    //podstawienie wyglądu do obsługi przez biblioteke
    var template = Handlebars.compile(source);
    //wyswietlenie templatki
    $(".row").html(source);
    $(".alert-danger").hide().css({"opacity": 0});

    var dane = {
        login: "",
        pass: "",
        plec: "",
        woj: "",
        dorosly: false,
        regulamin: true
    };
    //tworzymy własną funkcję którą możemy potem użyć $('...').plec()
    $.fn.extend({
        plec: function () {
            var val = "";
            this.each(function () {
                if ($(this).prop('checked')) {
                    val = $(this).val();
                }
            });
            return val;
        }
    });

    var rejestruj = function () {
        $(".bg-danger").parent().removeClass("bg-danger");
        $(".alert-danger").hide();
        var valid = true;

        //pobranie wartosci z pola w tym momęcie funckja val przybiera typ getera (val jest getterem i setterem)
        var login = $("#login").val();
        var pass = $("#pass").val();
        var plec = $("input[name='plec']").plec();
        var woj = $("#woj").find("option:selected").val();
        if (login === "" && (login.length < 3 || login.length > 15)) {
            $(".login-dan").show().animate({"opacity": 1});
            valid = false;
        }
        ;
        if (pass === "" && pass.length < 8) {
            $(".pass-dan").show().animate({"opacity": 1});
            valid = false;
        }
        ;
        if (plec === "") {
            $("input[name='plec']").parent().addClass("bg-danger");
            valid = false;
        }
        if (!$("#dorosly").prop('checked')) {
            $("#dorosly").parent().addClass("bg-danger");
            valid = false;
        }
        ;
        if (!$("#regulamin").prop('checked')) {
            $("#regulamin").parent().addClass("bg-danger");
            valid = false;
        }
        ;
        if (valid) {
            dane.login = login;
            dane.pass = pass;
            dane.plec = plec;
            dane.woj = woj;
            dane.dorosly = $("#dorosly").prop('checked');
            dane.regulamin = $("#regulamin").prop('checked');
        }
        return valid;
    }
    var potwierdzDane = function () {
        if (rejestruj()) {
            $(".row").animate({left: "-2500"}, "fast", function () {
                $(".row").css({left: "calc(250%)"})


                var source = $('#potwierdzenie').html();
                //podstawienie wyglądu do obsługi przez biblioteke
                var template = Handlebars.compile(source);
                //wyswietlenie templatki

                var html = template(dane);
                $(".row").html(html);
            })
            $(".row").animate({left: "0"}, "fast")
            $(document).on("click", "#wyslij", wyslij);
        }
    }
    $('#zarejestruj').on("click", potwierdzDane);

    var wyslij = function () {
        $.ajax({
            type: "POST",
            url: "http://jakis/adres",
            data: dane,
            success: function (ret) {
                //ten fragment wykona się po pomyślnym zakończeniu połączenia - gdy dostaliśmy odpowiedź od serwera nie będącą błędem (404, 400, 500 itp)
                //atrybut ret zawiera dane zwrócone z serwera
            },
            complete: function () {
                //ten fragment wykona się po zakończeniu łączenia - nie ważne czy wystąpił błąd, czy sukces
                var source = $('#powodzenie').html();
                //podstawienie wyglądu do obsługi przez biblioteke
                var template = Handlebars.compile(source);
                //wyswietlenie templatki
                $(".row").html(source);


            },
            error: function (jqXHR, errorText, errorThrown) {
                //ten fragment wykona się w przypadku BŁĘDU
                //do zmiennej errorText zostanie przekazany błąd
            }
        });
    }


});