$(function () {
    function typy() {
        var tekst = "";
        tekst += "Tekst " + typeof "Tekst"
        tekst += "<br/> 10 " + typeof 10
        tekst += "<br/> 10.5 " + typeof 10.5
        tekst += "<br/> true/false " + typeof true
        tekst += "<br/> {} " + typeof {marka: 'mercedes'}
        tekst += "<br/> [] tu powino być array " + typeof []
        tekst += "<br/> typ specialny null " + typeof null
        tekst += "<br/> typ specialny undefined " + typeof undefined
        tekst += "<br />";
        if (10 == "10") {
            tekst += "Zaszła automatyczne konwersja danych i interpretator sprawdził tylko wartość 10==\"10\" ";
            tekst += 10 == 10;

        }
        tekst += "<br />";
        if (10 === "10") {
        } else {
            tekst += "w tym momęcie interpretator przeglądarki sprawdził czy typ i wartość sie zgadza 10===\"10\" ";
            tekst += 10 === "10";
        }

        return tekst;
    }

    function petle() {
        var tekst = "";

        for (i = 0; i <= 10; i++) {
            tekst += i + " ";
        }
        for (i = 10; i >= 0; i--) {
            tekst += i + " ";
        }
        tekst += "<br />";
        var czesc = "czesc jestem while i wychodzę na false mogę nie zajść";
        napis = "";
        i = 0;
        while (i < czesc.length && napis != czesc) {
            napis += czesc[i];
            i++;
        }
        tekst += napis
        i = 1;
        tekst += "<br /><br />";
        tekst += "do while pętla która musi zawsze zajść ale może nigdy się nie skończyć <br />";

        do {
            i = i * 2;
            tekst += i + " ";
        } while (i < 100)


        return tekst;
    }

    function zmiene() {
        var zmiena = undefined;
        var tekst = "zmiene deklarujemy słowem var zmiena; zmiena ma podstawowy typ zawsze undefined <br/>";
        tekst += "zmiena=undefined jest typu " + zmiena;
        tekst += "<br />";
        zmiena = 10;
        tekst += "zmiena=10 jest typu " + typeof zmiena;
        tekst += "<br />";
        zmiena = true;
        tekst += "zmiena=true jest typu " + typeof zmiena;
        tekst += "<br />";
        zmiena = {};
        tekst += "zmiena={} jest typu " + typeof zmiena;
        tekst += "<br />";
        zmiena = [];
        tekst += "zmiena=[] jest typu " + typeof zmiena;
        tekst += "<br />";
        zmiena = null;
        tekst += "zmiena=null jest typu " + typeof zmiena;
        tekst += "<br />";
        tekst += "Dwa możliwe przypdaki pokazania działania interpretatora zmienych.<br /> jak myślisz jeżeli wykonam takie działanie? $(\"html\").text(z); var z=2; to się stanie? odp. poniżej.<br />";
        tekst += "odpowiedź: " + z;
        var z = 2;
        tekst += "<br />Tak, zmiena ma typ undefined. Dlaczego? interpretator przeglądarki odrazu jak znajdzie zmieną to ją deklaruje w window czyli ona już jest \"dostępna\"";
        return tekst;
    }

    function tablice() {
        var tekst = "";
        tekst += "Tablice w javascript mamy jedno wymiarowe oraz  asosciacyjne, tablice w javascript są dynamiczne.<br />";
        tekst += "Tablice deklarujemy na 2 sposoby var tab = [], var tab = new Array(), oczwyście przy deklaracji tablic mozemy wprowadzić pierwsze dane.<br />";
        tekst += "Zadeklarujemy tablice tab. z 10 cyframi. tablice mają pierwszy element w komurce 0<br />";
        var tab = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9];
        tekst += "Wyświetlmy tablicę tab, mamy dwa sposoby albo robimy to ręcznie co nie jest dobrym pomysłem lub za pomocą pętli jako że wiemy jak długa jest tablica. za pomocą \".length\" to użyjemy pętli krokowej for <br />";
        for (i = 0; i < tab.length; i++) {
            tekst += tab[i] + " ";
        }
        tekst += "<br />Dodajmy dynamicznie liczbe 10 za pomocą słowa tab.push(10)<br />";
        tab.push(10);
        for (i = 0; i < tab.length; i++) {
            tekst += tab[i] + " ";
        }
        tekst += "<br />Usuńmy pierwszy element tablicy za pomocą słowa .shift()<br />";
        tab.shift();
        for (i = 0; i < tab.length; i++) {
            tekst += tab[i] + " ";
        }
        tekst += "<br />Usuńmy ostatni element tablicy za pomocą słowa .pop()<br />";
        tab.pop();
        for (i = 0; i < tab.length; i++) {
            tekst += tab[i] + " ";

        }

        tab = [];

        tekst += "<br/> tablice asocicyjne inaczej skojażeniowe różnią się tym od zwykłych tablic, że zamiast odwoływać się do tablicy za pomocą liczb odwołujemy się za pomocą tkestu np. tab['kwiecien']=30; tab['maj']=31, najwygodniej do tych tablic dynamicznie odwołujemy się zmodyfikownym for-em<br/>";
        tab['kwiecien'] = 30;
        tab['maj'] = 31;
        for (m in tab) {
            tekst += m + " ma " + tab[m] + " dni </br>";
        }

        return tekst;
    }

    function obiety() {
        var tekst = "Obikety jest to bardzo ciekawa struktura w JavaScripcie są prawie identycznej jak wszędzie można je porównać do klas w Javie,C# czy innym języku np. stwórzym obiekt samochód. var samochod = {Nazwa:Lublin, Silnik:125cm3, Producent:FSL, wyswietl:function(){console.log(this.Nazwa+' '+this.Silnik+' '+this.Producent)}}; Jak się do tego odwołać. W javascript do elementów obiektu odwołujemy się \"kropką\" czyli jeżeli jesteśmy poza obiektem to oby wywołać element obiekcie użyjemy samochud.Nazwa, jezeli jesteśmy w obiekcie to zrobimy to za pomocą this.Nazwa;<br/>";
        var samochod = {
            Nazwa: "Lublin",
            Silnik: "125cm3",
            Producent: "FSL",
            wyswietl: function () {
                return this.Nazwa + '<br/>' + this.Silnik + '<br/>' + this.Producent
            }
        }
        tekst += samochod.wyswietl();


        return tekst;
    }

    $("#typy").html(typy());
    $("#petle").html(petle());
    $("#zmiene").html(zmiene());
    $("#tablice").html(tablice());
    $("#obiekty").html(obiety());

})