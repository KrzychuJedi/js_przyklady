//Utworzenie Obiektu do którego nie ma dostępu z poziomu window
(function(){
    //Pobieranie wartosci po ID
    var mojafunkcja = function getId(id)
    {
        return document.getElementById(id);
    }
    //Pobieranie watosci po class
    function getClass(clas)
    {
        return document.getElementsByClassName(clas);
    }
    //Pobieranie wartosci po name
    function getName(name)
    {
        return document.getElementsByName(name);
    }
    //Obiekt który jest widoczny z poziomu widnow
    sda={
        //Element obiektu sda do którego jest przypisana funkcja anoniowa
        field:function()
        {
            var mainDiv = document.getElementById('main');
            console.log(mainDiv.innerHTML);

            //Pobranie obiektu wyswietlacz
            var wyswwietlasz =  mojafunkcja("wyswietlacz");
            //pobranie elementu wyslij oraz podpięcie pod event onclick funkcji anoniomowej.
            mojafunkcja("wyslij").onclick = function()
            {
                //pobranie wartosci z inputu nazwa
                var nazwa = getId("nazwa").value;
                //Pobranie wartosci z inputu hasło który ma na liscie numer 0
                var haslo = getClass("haslo")[0].value;
                //wpisanie do diva textu
                wyswwietlasz.innerHTML = "Dane: "+nazwa+ " "+haslo;
            }
        }
    }

})()
sda.field();